# Psonic Challenger

This project utilizes Node.js with Express on the backend, written in JavaScript.
On the frontend, the page is served using HTML, styled 
using Bootstrap as a design library, and enhanced with custom CSS styles.

SCRIPT TO RUN THE project

npm i 
npm run dev:nodemon

