const express = require('express');
const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');
const Chart = require('chart.js');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));


// Serve the first page
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Handle form submission
app.post('/graph.html', (req, res) => {
//   Extract data from request
  const { clientname, risk1, risk2 } = req.body;

// Validate data
if(!clientname || isNaN(risk1) || isNaN(risk1) || risk1 > 10000 || risk1 < 1000 || risk2 > 20000 || risk2 < 10000){
    return res.status(400).send('400 Bad Request - The values for Risk 1 must fall within the range of 1000 to 10000, and the value for Risk 2 must be within the range of 10000 to 20000 in order to be considered valid.');
  }

// Process the data (e.g., write to file)
  const data = {
    clientname: {
      risk1: Number(risk1),
      risk2: Number(risk2)
    }
  };


  fs.writeFile('data.json', JSON.stringify(data), (err) => {
    if (err) throw err;
    console.log('Data saved to file');
    // Wait for 30 seconds before responding with setTimeout
    setTimeout(() => {
      res.redirect('/graph.html')
    }, 30000);
    //TODO: Dejar 30000 milisegundos como dice el challenger
  });
});

// Serve the second page with graph

  app.get('/data', (req, res) => {
    const data = [];

    fs.createReadStream('ema.csv')
        .pipe(csv())
        .on('data', (row) => {
            data.push(row);
        })
        .on('end', () => {
            res.json(data);
        });
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});